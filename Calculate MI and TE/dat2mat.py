import numpy as np
import numba as nb
import pickle
from scipy.io import loadmat, savemat
from random import gauss
from math import sqrt
import matplotlib as mpl
mpl.use('nbagg')
from mpl_toolkits.mplot3d import Axes3D
import time
import matplotlib.pyplot as plt
#%matplotlib notebook

def dat2mat(center=[], num_state=40):
    # Center of Circular Coordinate: [x, y]
    zebra_center = center
    NUMofStates = num_state
    encoder = 'zebrang'

    # Plot 2D Trajectory
    zebrapos = pickle.load(open("./zebrapos.dat","rb"))
    if np.shape(zebrapos)[0] != 2:
        raise Exception('Length of zebrapos.dat should be 2. Go back check your ImageJ.\n')
    zebra_center = np.double(zebra_center)
    zebra_B, zebra_A = np.array(zebrapos)[np.argsort([(zebrapos[0][1][0]-zebra_center[0])**2+(zebrapos[0][2][0]-zebra_center[1])**2, (zebrapos[1][1][0]-zebra_center[0])**2+(zebrapos[1][2][0]-zebra_center[1])**2])]
    plt.figure(figsize=(12,12))
    plt.plot(zebra_A[1]-zebra_center[0], zebra_A[2]-zebra_center[1], label='zebra_A')
    plt.plot(zebra_B[1]-zebra_center[0], zebra_B[2]-zebra_center[1], label='zebra_B')
    plt.legend()
    plt.show()
    plt.savefig('Traces.jpg')

    # Plot 1D Trajectory
    plt.figure()
    plt.plot(np.arange(10000,15000), zebra_A[1][10000:15000], label='zebra_A')
    plt.plot(np.arange(10000,15000), zebra_B[1][10000:15000], label='zebra_B')
    plt.legend()
    plt.xlabel('Frame')
    plt.ylabel('State')
    plt.show()
    plt.savefig('1D_Traces.jpg')

    # Save the mat file
    zebrang=[np.arctan2(zebra_A[2]-zebra_center[1], zebra_A[1]-zebra_center[0]), np.arctan2(zebra_B[2]-zebra_center[1], zebra_B[1]-zebra_center[0])]
    zebrang=[NUMofStates-((1-zebrang[0]/np.pi)*NUMofStates/2).astype(int), NUMofStates-((1-zebrang[1]/np.pi)*NUMofStates/2).astype(int)]
    zebra_path = "./fish_state" + "(" + str(NUMofStates) + "states).mat"
    savemat(zebra_path, {encoder:zebrang, 'hist': [np.histogram(zebrang[0], bins=np.arange(NUMofStates+1)+0.5), np.histogram(zebrang[1], bins=np.arange(NUMofStates+1)+0.5)]})