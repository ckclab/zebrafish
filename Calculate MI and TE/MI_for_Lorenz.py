import numpy as np
#import numba as nb
from random import gauss
from math import sqrt
import time

# using numpy's histogram2d to calculate the mutual information between two sequences
def mi_quick(a,b,d,bn=25):
    if d>0: xy,_,_ = np.histogram2d(a[d:],b[:-d],bn)
    elif d<0: xy,_,_ = np.histogram2d(a[:d],b[-d:],bn)
    else: xy,_,_ = np.histogram2d(a,b,bn)
    xy /= np.sum(xy)
    px = [np.array([max(x,1e-100) for x in np.sum(xy,axis=0)])]
    py = np.transpose([[max(x,1e-100) for x in np.sum(xy,axis=1)]])
    nxy = (xy/px)/py
    nxy[nxy==0] = 1e-100
    return np.sum(xy*np.log2(nxy))

def calc_ent(x):
    if np.ndim(x)==2:
        x_value_list, x_value_counts = np.unique(x, axis=0, return_counts=True)
        ent = 0.0
        for x_value in x_value_list:
            p = float(x_value_counts[np.where((x_value==x_value_list).all(axis=1))]) / len(x) 
            logp = np.log2(p)
            ent -= p * logp
    elif np.ndim(x)==1:
        x_value_list, x_value_counts = np.unique(x, axis=0, return_counts=True)
        ent = 0.0
        for x_value in x_value_list:
            p = float(x_value_counts[np.where(x_value==x_value_list)]) / len(x) 
            logp = np.log2(p)
            ent -= p * logp        
        
    return ent

def MI(y, x1):
    # MI(x1;y) = ent(x1) - ent(x1|y)
    # calc ent(x1|y)
    if np.ndim(y)==1:
        y_value_list, y_value_counts = np.unique(y, axis=0, return_counts=True)
        
        ent1 = 0.0
        
        for y_value in y_value_list:
            sub_x1 = x1[np.where(y==y_value)]
            temp_ent = calc_ent(sub_x1)
            p = float(y_value_counts[np.where(y_value==y_value_list)]) / len(y)
            ent1 += p * temp_ent 
    elif np.ndim(y)==2:
        y_value_list, y_value_counts = np.unique(y, axis=0, return_counts=True)
        
        ent1 = 0.0
        
        for y_value in y_value_list:
            sub_x1 = x1[np.where((y==y_value).all(axis=1))]
            temp_ent = calc_ent(sub_x1)
            p = float(y_value_counts[np.where((y_value==y_value_list).all(axis=1))]) / len(y)
            ent1 += p * temp_ent        
    
    return calc_ent(x1) - ent1

def cond_MI(x_cond, y_cond, xt):
    # conditional MI(x_cond;xt|y_cond) = ent(x_cond|y_cond) - ent(x_cond|xt,y_cond)
    
    # construct xy_cond
    xt = np.reshape(xt, (1,len(xt)))
    xy_cond = np.concatenate((y_cond, xt.T), axis=1)

    y_value_list, y_value_counts = np.unique(y_cond, axis=0, return_counts=True)
    xy_value_list, xy_value_counts = np.unique(xy_cond, axis=0, return_counts=True)
    ent1 = 0.0
    ent2 = 0.0
    
    # calculate H(x_cond|y_cond)
    for y_value in y_value_list:
        sub_x = x_cond[np.where((y_cond==y_value).all(axis=1))]
        temp_ent = calc_ent(sub_x)
        py = float(y_value_counts[np.where((y_value==y_value_list).all(axis=1))]) / len(y_cond)
        ent1 += py * temp_ent

    # calculate H(x_cond|xt, y_cond)
    for xy_value in xy_value_list:
        sub_x2 = x_cond[np.where((xy_cond==xy_value).all(axis=1))]
        temp_ent2 = calc_ent(sub_x2)
        pxy = float(xy_value_counts[np.where((xy_value==xy_value_list).all(axis=1))]) / len(xy_cond)      
        ent2 += pxy * temp_ent2

    return ent1 - ent2

def lorenz_sys(kx1, kx2, nstate=40, npts=100000, sigma=10.0, rho=28.0, beta=8/3.0):
    # parameters for the Lorenz system
    #npts = 100000
    x1 = np.zeros(npts)
    y1 = np.zeros(npts)
    z1 = np.zeros(npts)

    x2 = np.zeros(npts)
    y2 = np.zeros(npts)
    z2 = np.zeros(npts)

    #sigma = 10.0
    #rho = 28.
    #beta = 8/3.
    dt = 0.005

    x1[0] = -7.0
    y1[0] = 0.1
    z1[0] = -1.0

    x2[0] = -7.1
    y2[0] = 0.2
    z2[0] = -0.9

    '''
    x2[0] = -7.0
    y2[0] = 0.1
    z2[0] = -1.0

    x1[0] = -7.1
    y1[0] = 0.2
    z1[0] = -0.9
    '''

    dt = 0.001
    tau = 0.05
    delay = int(tau/dt)

    # two non-interacting Lorenz systems
    # from t =0 to t= delay
    for i in range(1,delay+1):
        dx1 = sigma*(y1[i-1]-x1[i-1])*dt
        dy1 = (x1[i-1]*(rho - z1[i-1]) - y1[i-1])*dt
        dz1 = (x1[i-1]*y1[i-1] - beta*z1[i-1])*dt
        
        x1[i] = x1[i-1] + dx1
        y1[i] = y1[i-1] + dy1
        z1[i] = z1[i-1] + dz1

        dx2 = sigma*(y2[i-1]-x2[i-1] )*dt
        dy2 = (x2[i-1]*(rho - z2[i-1]) - y2[i-1])*dt
        dz2 = (x2[i-1]*y2[i-1] - beta*z2[i-1])*dt
        
        x2[i] = x2[i-1] + dx2
        y2[i] = y2[i-1] + dy2
        z2[i] = z2[i-1] + dz2

    # two Lorenz systems with delay interaction from 1 to 2
    # only in the x direction starting from t = delay+1
    #kx1 = 1.0
    kx1 = float(kx1)
    ky1 = 0.0
    kz1 = 0.0

    #kx2 = 0.0
    kx2 = float(kx2)
    ky2 = 0.0
    kz2 = 0.0

    for i in range(delay+1,npts):
        int_x12 = kx2*(x1[i-1]-x2[i-delay-1])
        int_y12 = ky2*(y1[i-1]-y2[i-delay-1])
        int_z12 = kz2*(z1[i-1]-z2[i-delay-1])
        
        dx2 = sigma*(y2[i-1]-x2[i-1] +             int_x12)*dt
        dy2 = (x2[i-1]*(rho - z2[i-1]) - y2[i-1] + int_y12)*dt
        dz2 = (x2[i-1]*y2[i-1] - beta*z2[i-1] +    int_z12)*dt
        
        x2[i] = x2[i-1] + dx2
        y2[i] = y2[i-1] + dy2
        z2[i] = z2[i-1] + dz2

    #for i in range(delay,npts):    
        
        int_x21 = kx1*(x2[i-1]-x1[i-delay-1])
        int_y21 = ky1*(y2[i-1]-y1[i-delay-1])
        int_z21 = kz1*(z2[i-1]-z1[i-delay-1])
        
        dx1 = sigma*(y1[i-1]-x1[i-1] +             int_x21)*dt
        dy1 = (x1[i-1]*(rho - z1[i-1]) - y1[i-1] + int_y21)*dt
        dz1 = (x1[i-1]*y1[i-1] - beta*z1[i-1] +    int_z21)*dt
        
        x1[i] = x1[i-1] + dx1
        y1[i] = y1[i-1] + dy1
        z1[i] = z1[i-1] + dz1

    #generation of data
    #nstate= 40

    x = np.interp(x1, (x1.min(), x1.max()), (0, nstate-1))
    x = x.astype(int)
    y = np.interp(x2, (x2.min(), x2.max()), (0, nstate-1))
    y = y.astype(int)

    return x, y


def build_x1_x2_y(x, y, Lx=25, Ly=25, tau_y=0):
#Lx = 25
#Ly = 25
#tau_y = -500

    x_cond = np.array([x[i:i+Lx] for i in range(0,len(x)-Lx)])
    y_cond = np.array([y[i:i+Ly] for i in range(0,len(y)-Ly)])
    xt = np.array([x[i+Lx] for i in range(0, len(x)-Lx)])

    if tau_y > 0:
        x_cond = x_cond[tau_y:]
        y_cond = y_cond[:len(x_cond)]
        xt = xt[tau_y:]
        if len(x_cond) > len(y_cond):
            x_cond = x_cond[:len(y_cond)]
            xt = xt[:len(y_cond)]
        elif len(x_cond) < len(y_cond):
            y_cond = y_cond[:len(x_cond)]
        print("tau_y = ", tau_y)

    elif tau_y < 0:
        print("tau_y = ", tau_y)
        tau_y = -tau_y
        y_cond = y_cond[tau_y:]
        x_cond = x_cond[:len(y_cond)]
        xt = xt[:len(y_cond)]
        if len(x_cond) > len(y_cond):
            x_cond = x_cond[:len(y_cond)]
            xt = xt[:len(y_cond)]
        elif len(x_cond) < len(y_cond):
            y_cond = y_cond[:len(x_cond)]

    elif tau_y == 0:
        print("tau_y = ", tau_y)
        if len(x_cond) > len(y_cond):
            #y_cond = y_cond      
            d = len(x_cond) - len(y_cond)
            x_cond = x_cond[d:]
            xt = xt[d:]
        else:
            #x_cond = x_cond
            #xt = xt
            d = len(y_cond) - len(x_cond)
            y_cond = y_cond[d:]

    x1 = x_cond
    x2 = y_cond

    return x1, x2, xt
####################################################################################



