from numba import jit
import numpy as np
from scipy import io
import time

# Calculate entropy
#@jit
def calc_ent(x):

    x_value_list = set(x)
    ent = 0.0
    for x_value in x_value_list:
        p = float(len(x[np.where(x==x_value)[0]])) / len(x)
        logp = np.log2(p)
        ent -= p * logp

    return ent 

# Calculate H(X|Y1)-H(X|Y1, Y2)
#@jit(nopython=True)
def two_cond_ent(x_cond, y_cond, xt):
    # calc ent(xt|x_cond, y_cond)
    
    # build xy_cond | np.concatenate can do the same
    """
    xy_cond1 = np.zeros((len(x_cond),Lx+Ly))
    for row in range(len(xy_cond1)):
        xy_cond1[row] = np.append(x_cond[row], y_cond[row])
    """
    xy_cond = np.concatenate((x_cond, y_cond), axis=1)

    x_value_list, x_value_counts = np.unique(x_cond, axis=0, return_counts=True)
    xy_value_list, xy_value_counts = np.unique(xy_cond, axis=0, return_counts=True)
    ent1 = 0.0
    ent2 = 0.0
    print(np.shape(x_value_list)[0], np.shape(x_cond)[0], "x%: ", np.shape(x_value_list)[0] / np.shape(x_cond)[0])
    print(len(xy_value_list), len(xy_cond), "xy%: ", len(xy_value_list) / len(xy_cond))
    
    # calculate H(X_t|X_his)
    #c1 = 0
    #print(len(x_value_list))
    for x_value in x_value_list:
        sub_xt = xt[np.where((x_cond==x_value).all(axis=1))]
        temp_ent = calc_ent(sub_xt)
        px = float(x_value_counts[np.where((x_value==x_value_list).all(axis=1))]) / len(x_cond)
        ent1 += px * temp_ent
        #c1+=1
        #print(c1)


    # calculate H(X_t|X_his, Y_his)
    #c2=0
    #print(len(xy_value_list))
    for xy_value in xy_value_list:
        sub_xt2 = xt[np.where((xy_cond==xy_value).all(axis=1))]
        temp_ent2 = calc_ent(sub_xt2)
        pxy = float(xy_value_counts[np.where((xy_value==xy_value_list).all(axis=1))]) / len(xy_cond)      
        ent2 += pxy * temp_ent2
        #c2+=1
        #print(c2)


    #print("ent1: ", ent1)
    #print("ent2: ", ent2)
    return ent1 - ent2

# Calculate TE with parameters: Lx, Ly, tau, x and y  
def transfer_entropy(Lx, Ly, tau_x, tau_y, x, y):
    x_cond = np.array([x[i:i+Lx] for i in range(0,len(x)-Lx)])
    y_cond = np.array([y[i:i+Ly] for i in range(0,len(y)-Ly)])
    xt = np.array([x[i+Lx+tau_x] for i in range(0, len(x)-Lx)])
    
    if tau_y > 0:
        x_cond = x_cond[tau_y:]
        y_cond = y_cond[:len(x_cond)]
        xt = xt[tau_y:]
        if len(x_cond) > len(y_cond):
            x_cond = x_cond[:len(y_cond)]
            xt = xt[:len(y_cond)]
        elif len(x_cond) < len(y_cond):
            y_cond = y_cond[:len(x_cond)]
        print(tau_y)

    elif tau_y < 0:
        print(tau_y)
        tau_y = -tau_y
        y_cond = y_cond[tau_y:]
        x_cond = x_cond[:len(y_cond)]
        xt = xt[:len(y_cond)]
        if len(x_cond) > len(y_cond):
            x_cond = x_cond[:len(y_cond)]
            xt = xt[:len(y_cond)]
        elif len(x_cond) < len(y_cond):
            y_cond = y_cond[:len(x_cond)]

    elif tau_y == 0:
        print(tau_y)
        if len(x_cond) > len(y_cond):
            #y_cond = y_cond      
            d = len(x_cond) - len(y_cond)
            x_cond = x_cond[d:]
            xt = xt[d:]
        else:
            #x_cond = x_cond
            #xt = xt
            d = len(y_cond) - len(x_cond)
            y_cond = y_cond[d:]

    #print(len(x_cond))
    #print(len(y_cond))
    #print(len(xt))
    te = two_cond_ent(x_cond, y_cond, xt)
    
    return te

def main():
    mat = io.loadmat('./zebra_test1/zebrang(40states).mat')

    A = mat['zebrang'][0]
    B = mat['zebrang'][1]

    x = B
    y = A
    Lx = 32
    Ly = 42
    tau_x = 0
    tau_y = -84
    
    te = transfer_entropy(Lx, Ly, tau_x, tau_y, x, y)
    return te

if __name__ == "__main__":
    main()

