from ctypes import c_bool

import pygame
import pandas as pd
import numpy as np
import tkinter as tk
from multiprocessing import Process, Queue, Value, Array  # multi process to aviod imshow be effected

## Data Analysis ######################################################
filename = './F_F_03DeepCut_resnet50_test_1_gpuOct9shuffle1_200000.csv'
df = pd.read_csv(filename)

frame_center = [379, 371]

df_out = pd.DataFrame()
df_out = df_out.assign(frame = df['scorer'][2:],
              head_o_x = df[filename[-50:-4]][2:],
              head_o_y = df[filename[-50:-4]+'.1'][2:],
              tail_o_x = df[filename[-50:-4]+'.3'][2:],
              tail_o_y = df[filename[-50:-4]+'.4'][2:],
              head_i_x = df[filename[-50:-4]+'.6'][2:],
              head_i_y = df[filename[-50:-4]+'.7'][2:],
              tail_i_x = df[filename[-50:-4]+'.9'][2:],
              tail_i_y = df[filename[-50:-4]+'.10'][2:],
             )

df_out = df_out.astype('float64')

df_out['head_i_x'] = df_out['head_i_x'] - frame_center[0]
df_out['head_i_y'] = df_out['head_i_y'] - frame_center[1]
df_out['head_o_x'] = df_out['head_o_x'] - frame_center[0]
df_out['head_o_y'] = df_out['head_o_y'] - frame_center[1]
df_out['tail_i_x'] = df_out['tail_i_x'] - frame_center[0]
df_out['tail_i_y'] = df_out['tail_i_y'] - frame_center[1]
df_out['tail_o_x'] = df_out['tail_o_x'] - frame_center[0]
df_out['tail_o_y'] = df_out['tail_o_y'] - frame_center[1]
# compute fish center
df_out['center_i_x'] = (df_out['head_i_x'] + df_out['tail_i_x'])/2
df_out['center_i_y'] = (df_out['head_i_y'] + df_out['tail_i_y'])/2
df_out['center_o_x'] = (df_out['head_o_x'] + df_out['tail_o_x'])/2
df_out['center_o_y'] = (df_out['head_o_y'] + df_out['tail_o_y'])/2
# compute R
df_out['R_head_i'] = np.sqrt(df_out['head_i_x']**2+df_out['head_i_y']**2)
df_out['R_tail_i'] = np.sqrt(df_out['tail_i_x']**2+df_out['tail_i_y']**2)
df_out['R_head_o'] = np.sqrt(df_out['head_o_x']**2+df_out['head_o_y']**2)
df_out['R_tail_o'] = np.sqrt(df_out['tail_o_x']**2+df_out['tail_o_y']**2)
df_out['R_center_i'] = np.sqrt(df_out['center_i_x']**2+df_out['center_i_y']**2)
df_out['R_center_o'] = np.sqrt(df_out['center_o_x']**2+df_out['center_o_y']**2)
# compute theta
df_out['theta_head_i'] = np.arctan2(df_out['head_i_x'], df_out['head_i_y'])
df_out['theta_tail_i'] = np.arctan2(df_out['tail_i_x'], df_out['tail_i_y'])
df_out['theta_head_o'] = np.arctan2(df_out['head_o_x'], df_out['head_o_y'])
df_out['theta_tail_o'] = np.arctan2(df_out['tail_o_x'], df_out['tail_o_y'])
df_out['theta_center_i'] = np.arctan2(df_out['center_i_x'], df_out['center_i_y'])
df_out['theta_center_o'] = np.arctan2(df_out['center_o_x'], df_out['center_o_y'])
# compute fish vector
df_out['fv_i_x'] = df_out['head_i_x']-df_out['tail_i_x']
df_out['fv_i_y'] = df_out['head_i_y']-df_out['tail_i_y']
df_out['fv_o_x'] = df_out['head_o_x']-df_out['tail_o_x']
df_out['fv_o_y'] = df_out['head_o_y']-df_out['tail_o_y']
# compute phi 
df_out['phi_i'] = np.arctan2(df_out['fv_i_y'], df_out['fv_i_x'])
df_out['phi_o'] = np.arctan2(df_out['fv_o_y'], df_out['fv_o_x'])
# compute fish length
df_out['length_i'] = np.sqrt(df_out['fv_i_x']**2+df_out['fv_i_y']**2)
df_out['length_o'] = np.sqrt(df_out['fv_o_x']**2+df_out['fv_o_y']**2)
# compute delta
df_out['delta_i'] = np.arccos((df_out['fv_i_x']/df_out['length_i'])*(df_out['center_i_y']/df_out['R_center_i'])
                              -(df_out['fv_i_y']/df_out['length_i'])*(df_out['center_i_x']/df_out['R_center_i']))
df_out['delta_o'] = np.arccos((df_out['fv_o_x']/df_out['length_o'])*(df_out['center_o_y']/df_out['R_center_o'])
                              -(df_out['fv_o_y']/df_out['length_o'])*(df_out['center_o_x']/df_out['R_center_o']))
# shift to 0~2pi
df_out.loc[((df_out['fv_i_x'])*(df_out['center_i_x'])+(df_out['fv_i_y'])*(df_out['center_i_y'])) <= 0, 'delta_i'] = 2*np.pi - df_out['delta_i']
df_out.loc[((df_out['fv_o_x'])*(df_out['center_o_x'])+(df_out['fv_o_y'])*(df_out['center_o_y'])) <= 0, 'delta_o'] = 2*np.pi - df_out['delta_o']
# compute shift delta(pi~-pi)
df_out['delta_i_shift'] = df_out['delta_i']
df_out.loc[df_out['delta_i'] > np.pi, 'delta_i_shift'] = df_out['delta_i'] - 2*np.pi
df_out['delta_o_shift'] = df_out['delta_o']
df_out.loc[df_out['delta_o'] > np.pi, 'delta_o_shift'] = df_out['delta_o'] - 2*np.pi
# compute rho
df_out['rho_i'] = df_out['delta_i'] - np.pi/2
df_out['rho_o'] = df_out['delta_o'] - np.pi/2
df_out.loc[df_out['rho_i'] < 0, 'rho_i'] = df_out['rho_i'] + 2*np.pi
df_out.loc[df_out['rho_o'] < 0, 'rho_o'] = df_out['rho_o'] + 2*np.pi
###############################################################################################

col_label = {}
for label in df_out.columns:
    col_label[label] = np.where(df_out.columns==label)[0][0]

data = df_out.values
R_o = data[:,col_label['R_center_o']]
theta_o = data[:,col_label['theta_center_o']]
fish_len_o = data[:,col_label['length_o']]
posture_o = [1 if theta_o[(i+1)%len(theta_o)]-theta_o[i%len(theta_o)]>0 else -1 for i in range(len(theta_o))]
posture_o_temp = posture_o.copy()
for i in range(len(posture_o)):
    if posture_o[i] - posture_o[(i+1)%len(posture_o)] == 2:
        if posture_o[i] - posture_o[(i+16)%len(posture_o)] == 0:
            posture_o[(i+1)%len(posture_o)] = 1
            posture_o[(i+2)%len(posture_o)] = 1
            posture_o[(i+3)%len(posture_o)] = 1
            posture_o[(i+4)%len(posture_o)] = 1
            posture_o[(i+5)%len(posture_o)] = 1
            posture_o[(i+6)%len(posture_o)] = 1
            posture_o[(i+7)%len(posture_o)] = 1
            posture_o[(i+8)%len(posture_o)] = 1
            posture_o[(i+9)%len(posture_o)] = 1
            posture_o[(i+10)%len(posture_o)] = 1
            posture_o[(i+11)%len(posture_o)] = 1
            posture_o[(i+12)%len(posture_o)] = 1
            posture_o[(i+13)%len(posture_o)] = 1
            posture_o[(i+14)%len(posture_o)] = 1
            posture_o[(i+15)%len(posture_o)] = 1
        else:
            posture_o[(i-7)%len(posture_o)] = 5
            posture_o[(i-6)%len(posture_o)] = 10
            posture_o[(i-5)%len(posture_o)] = 15
            posture_o[(i-4)%len(posture_o)] = 20
            posture_o[(i-3)%len(posture_o)] = 25
            posture_o[(i-2)%len(posture_o)] = 30
            posture_o[(i-1)%len(posture_o)] = 35
            posture_o[(i)%len(posture_o)] = 40
            posture_o[(i+1)%len(posture_o)] = 45
            posture_o[(i+2)%len(posture_o)] = 50
            posture_o[(i+3)%len(posture_o)] = 55
            posture_o[(i+4)%len(posture_o)] = 60
            posture_o[(i+5)%len(posture_o)] = 65
            posture_o[(i+6)%len(posture_o)] = 70
            posture_o[(i+7)%len(posture_o)] = 75
    elif posture_o[i] - posture_o[(i+1)%len(posture_o)] == -2:
        if posture_o[i] - posture_o[(i+14)%len(posture_o)] == 0:
            posture_o[(i+1)%len(posture_o)] = -1
            posture_o[(i+2)%len(posture_o)] = -1
            posture_o[(i+3)%len(posture_o)] = -1
            posture_o[(i+4)%len(posture_o)] = -1
            posture_o[(i+5)%len(posture_o)] = -1
            posture_o[(i+6)%len(posture_o)] = -1
            posture_o[(i+7)%len(posture_o)] = -1
            posture_o[(i+8)%len(posture_o)] = -1
            posture_o[(i+9)%len(posture_o)] = -1
            posture_o[(i+10)%len(posture_o)] = -1
            posture_o[(i+11)%len(posture_o)] = -1
            posture_o[(i+12)%len(posture_o)] = -1
            posture_o[(i+13)%len(posture_o)] = -1
        else:
            posture_o[(i-6)%len(posture_o)] = -5
            posture_o[(i-5)%len(posture_o)] = -10
            posture_o[(i-4)%len(posture_o)] = -15
            posture_o[(i-3)%len(posture_o)] = -20
            posture_o[(i-2)%len(posture_o)] = -25
            posture_o[(i-1)%len(posture_o)] = -30
            posture_o[(i)%len(posture_o)] = -35
            posture_o[(i+1)%len(posture_o)] = -40
            posture_o[(i+2)%len(posture_o)] = -45
            posture_o[(i+3)%len(posture_o)] = -50
            posture_o[(i+4)%len(posture_o)] = -55
            posture_o[(i+5)%len(posture_o)] = -60
            posture_o[(i+6)%len(posture_o)] = -65

#print('p :', posture_o[:1000])
#print('pt: ', posture_o_temp[:1000])

class Fish(pygame.sprite.Sprite):

    def __init__(self, pos=(0, 0), filepath="./3D/0057.png", scale=1.):
        super(Fish, self).__init__()
        self.ori_image = pygame.image.load(filepath)
        self.resize_image = pygame.transform.scale(self.ori_image, (400,200))
        self.CW_image = pygame.transform.flip(self.resize_image, False, True)
        self.CCW_image = pygame.transform.flip(self.CW_image, True, False)
        self.turn_image = {}
        self.flip_turn_image = {}
        k=1
        for i in range(61,76):
            self.turn_image[k] = pygame.transform.flip(pygame.transform.scale(pygame.image.load("./3D/00"+str(i)+".png"), (400,200)), False, True)
            k+=1
        k=-1
        for i in range(287,300):
            self.flip_turn_image[k] = pygame.transform.flip(pygame.transform.scale(pygame.image.load("./3D/0"+str(i)+".png"), (400,200)), False, True)
            k-=1
        self.image = self.CW_image
        self.rect = self.image.get_rect()
        self.rect.center = pos
        self.scale = scale

    def update(self):
        self.image = pygame.transform.rotate(self.original_image, self.angle)
        self.angle += 1 % 360
        x, y = self.rect.center  # Save its current center.
        self.rect = self.image.get_rect()  # Replace old rect with new rect.
        self.rect.center = (x, y)  # Put the new rect's center at old center.

    def up_Pos(self, pos, angle, CW=1):  # use x-dir as axis and counterclockwise as positive
        if CW == 1:
            image = self.CW_image
        elif CW == -1:
            image = self.CCW_image
        elif CW>1:
            image = self.turn_image[CW/5]
        elif CW<-1:
            image = self.flip_turn_image[CW/5]
        self.image = pygame.transform.rotozoom(image, angle, self.scale)
        self.rect = self.image.get_rect()  # Replace old rect with new rect.
        self.rect.center = pos  # Put the new rect's center at old center.

    def change_scale(self, scale):
        self.scale = scale


def moving_fish(S, O, clockwise, Radius):
    pygame.init()
    pygame.display.set_caption("OpenCV camera stream on Pygame")
    clock = pygame.time.Clock()
    scree_size = np.array([1280, 1024])
    #screen = pygame.display.set_mode(scree_size)  # , pygame.DOUBLEBUF | pygame.HWSURFACE)  #pygame.FULLSCREEN
    screen = pygame.display.set_mode((0, 0), pygame.RESIZABLE)

    bg = pygame.image.load('bg8.png')

    fish = Fish(pos=(0, 0), filepath="./3D/0057.png", scale=0.5)
    screen.fill([0, 0, 0])
    light = 87   #200
    angle = 0
    i = 0
    R = Radius.value
    #R = Radius[i]
    center = (scree_size/2).astype('int')
    print(center)
    #CW = clockwise.value
    CW = clockwise[i]
    omega = O.value

    # 關閉程式的程式碼
    running = True
    while running:
        for event in pygame.event.get():
            keys = pygame.key.get_pressed()
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    running = False
                """
				if event.key == pygame.K_c:
					CW *= -1
			if keys[pygame.K_EQUALS]:
				omega += 10
			if keys[pygame.K_MINUS]:
				omega -= 10
				if omega < 0:
					omega = 0
				"""
        #for i in range(len(Radius)):
        # update the value
        omega = O.value
        #R = Radius.value
        #R = Radius[i]
        #CW = clockwise.value
        CW = clockwise[i]

        # get time dash of last frame in millisecond
        #time = clock.tick_busy_loop()
        clock.tick(30)
        # calculate the displacement
        #angle += CW * omega * time / 1000
        #theta = angle / 360 * 2 * np.pi
        #i += 1
        '''
        if i==1:
            theta = theta_o[i]
            angle = int(np.degrees(theta))%360

        #R = Radius[i]
        
        if theta_o[i] - theta > 0:
            CW_temp = 1
            if CW_temp * CW >= 0:
                CW = CW_temp
            elif CW_temp * CW < 0:
                CW = np.inf
                print(i)
        else:
            CW_temp = -1
            if CW_temp * CW > 0:
                CW = CW_temp
            elif CW_temp * CW < 0:
                CW = -np.inf
                print(i)
        '''
        theta = theta_o[i]
        angle = int(np.degrees(theta))%360
        # turn into X-Y coordinate
        width = center[0] + int(R * np.sin(theta))
        height = center[1] - int(R * np.cos(theta))
        marker_pos = (center[0] + int((R+100) * np.sin(theta)), center[1] - int((R+100) * np.cos(theta)))
        # update fish position
        fish.up_Pos((width, height), -angle, CW)
        screen.fill([light, light, light])
        #screen.blit(bg, (290, 165))
        screen.blit(fish.image, fish.rect)
        pygame.draw.circle(screen, (0,0,0), center, 10, 0)
        pygame.draw.circle(screen, (0,0,0), center, 340, 1)
        pygame.draw.circle(screen, (0,0,0), marker_pos, 30, 0)
        pygame.display.update()
        i+=1
    pygame.quit()


def show_console(scale, omega, CW, Radius):
    # 讀入影片,由於參數傳遞的關係，數據必須先預處理成list才能避免傳遞時使用傳址的方式
    # 第1步，例項化object，建立視窗window
    window = tk.Tk()

    # 第2步，給視窗的視覺化起名字
    window.title('console panel')

    # 第3步，設定視窗的大小(長 * 寬)
    window.geometry('500x300')  # 這裡的乘是小x

    # 第4步，在圖形介面上設定標籤
    var = tk.StringVar()  # 將label標籤的內容設定為字元型別，用var來接收hit_me函式的傳出內容用以顯示在標籤上
    Label1 = tk.Label(window, textvariable=var, bg='green', fg='white', font=('Arial', 12), width=30, height=2)
    # 說明： bg為背景，fg為字型顏色，font為字型，width為長，height為高，這裡的長和高是字元的長和高，比如height=2,就是標籤有2個字元這麼高
    Label1.pack()

    dir = tk.StringVar()
    # 定義一個函式功能（內容自己自由編寫），供點選Button按鍵時呼叫，呼叫命令引數command=函式名
    Rot = tk.Button(window, textvariable=dir, text="Right", font=('Arial', 12), width=5, height=1,
                    command=lambda: Clockwise(CW, dir))
    Rot.pack()

    Speed_bar = tk.Scale(window, label="Speed", from_=0, to=360, orient=tk.HORIZONTAL,
                         length=400, showvalue=True, tickinterval=60,
                         command=lambda v: Speed(omega, int(v)))
    Speed_bar.pack()

    #Radius_bar = tk.Scale(window, label='Radius', from_=100, to=600, orient=tk.HORIZONTAL,
    #                      length=400, showvalue=True, tickinterval=50, resolution=1,
    #                      command=lambda v: getR(Radius, int(v)))
    #Radius_bar.pack()

    # 第6步，主視窗迴圈顯示
    window.mainloop()


# 注意，loop因為是迴圈的意思，window.mainloop就會讓window不斷的重新整理，如果沒有mainloop,就是一個靜態的window,傳入進去的值就不會有迴圈，mainloop就相當於一個很大的while迴圈，有個while，每點選一次就會更新一次，所以我們必須要有迴圈
# 所有的視窗檔案都必須有類似的mainloop函式，mainloop是視窗檔案的關鍵的關鍵。

def Speed(omega, v):
    omega.value = v


def getR(Radius, v):
    Radius.value = v


def Clockwise(CW, dir):
    CW.value *= -1
    temp = CW.value
    dir.set("Reverse")


if __name__ == '__main__':
    scale = Value("d", 1)
    omega = Value("d", 10)
    #CW = Value("i", 1)
    CW = Array("d", posture_o)
    Radius = Value("d", 300)
    #Radius = Array("d", R_o)
    window = Process(target=moving_fish, args=(scale, omega, CW, Radius,))
    #console = Process(target=show_console, args=(scale, omega, CW, Radius,))
    window.start()
    console.start()
    console.join()  # 等待console結束，主程序才會繼續
    window.terminate()  # 一旦console join, 摧毀window程序
